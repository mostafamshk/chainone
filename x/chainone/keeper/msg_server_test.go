package keeper_test

import (
	"context"
	"testing"

	keepertest "chainone/testutil/keeper"
	"chainone/x/chainone/keeper"
	"chainone/x/chainone/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
)

func setupMsgServer(t testing.TB) (types.MsgServer, context.Context) {
	k, ctx := keepertest.ChainoneKeeper(t)
	return keeper.NewMsgServerImpl(*k), sdk.WrapSDKContext(ctx)
}
